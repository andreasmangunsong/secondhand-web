<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Non Gherkin Update Product</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f3685f65-2ebb-4a58-bac1-efbbdf036fd9</testSuiteGuid>
   <testCaseLink>
      <guid>c4ce2ef1-415a-4e30-8f13-264f6e8b1730</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Samuel/Step Definition/Feature Update Product/UPD01 - User wants to update a product they have</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>023538bf-26d1-444f-88fb-578f0bdff84e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>30a1fe4d-9399-480f-8635-f4abb7f1f78c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Samuel/Step Definition/Feature Update Product/UPD02 - User wants to update without enter any data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>93ce647b-3686-47ee-a113-3f772af6e650</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Samuel/Step Definition/Feature Update Product/UPD03 - User wants to update product, but input a negative number in the price field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e1f44218-a5ed-49f1-a867-991d41640944</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
