<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Negative</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>123c786a-4af8-4ae8-a7ea-bccb67e74538</testSuiteGuid>
   <testCaseLink>
      <guid>cc07aab2-46e4-4dc2-8d96-60f24842f8ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Andreas/Step Definition/Feature Login/LOG02 - User want to login without input email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>820ae630-42f4-4867-8d7f-99352ba5211e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ef97194f-f302-464a-ad77-ab8c1100ede0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c623f7ad-b52e-40f0-890e-04ebbf0fe6ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Andreas/Step Definition/Feature Login/LOG03 - User want to login using correct email, but incorrect password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a53a6757-58c4-4f8c-be6e-0317905783ad</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8f34eb80-9ff0-4893-95a5-803f660ab9d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Andreas/Step Definition/Feature Login/LOG04 - User want to login using incorrect email, but correct password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>43e99e96-6ea2-45b2-84be-43750abd344e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46e39027-7cbe-4ec4-a33b-f7b39f1650fc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f561955a-7c5f-44f5-a680-fb9b0d1ddce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Andreas/Step Definition/Feature Login/LOG05 - User want to login using incorrect email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c27d8d11-2ed3-463e-839f-26d77b27586a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>29211b8b-af99-426f-b5f4-60b06daa62e4</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
