$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/UpdateProduct.feature");
formatter.feature({
  "name": "Update Product",
  "description": "\tUser wants to edit a product they have published before",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "UPD01 - User wants Update their Product with valid data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@UpdateProduct"
    },
    {
      "name": "@UPD01"
    },
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "user successfully log in to the web",
  "keyword": "Given "
});
formatter.match({
  "location": "UpdateProduct.user_successfully_log_in_to_the_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user navigated to product detail view",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_navigated_to_product_detail_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product name",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product price with \"456000\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_price_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product category",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product description with \"edit positive\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_description_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product image",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button preview",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_click_button_preview()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button terbitkan from product view",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_click_button_terbitkan_from_product_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Product Updated and user directed to product view",
  "keyword": "Then "
});
formatter.match({
  "location": "UpdateProduct.user_directed_to_product_view()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "UPD02 - User wants to update without enter any data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@UpdateProduct"
    },
    {
      "name": "@UPD02"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "user successfully navigated and log in to the web",
  "keyword": "Given "
});
formatter.match({
  "location": "UpdateProduct.user_successfully_navigated_and_log_in_to_the_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user navigated to product detail view",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_navigated_to_product_detail_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product name with \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_name_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product price with \"\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_price_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user does not input product category",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_does_not_input_product_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product description with \"\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_description_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button terbitkan",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_click_button_terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "error message will appear to validate empty fields",
  "keyword": "Then "
});
formatter.match({
  "location": "UpdateProduct.error_message_will_appear_to_validate_empty_fields()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "UPD03 - User wants to update product, but input a negative number in the price field",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@UpdateProduct"
    },
    {
      "name": "@UPD03"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "user successfully navigated and log in to the web",
  "keyword": "Given "
});
formatter.match({
  "location": "UpdateProduct.user_successfully_navigated_and_log_in_to_the_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user navigated to product detail view",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_navigated_to_product_detail_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product name with \"Harga negative\"",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_name_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product price with \"-4321\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_price_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product category",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product description with \"test harga negative\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_description_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button terbitkan",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_click_button_terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Product Updated and user directed to product view",
  "keyword": "Then "
});
formatter.match({
  "location": "UpdateProduct.user_directed_to_product_view()"
});
formatter.result({
  "status": "passed"
});
});