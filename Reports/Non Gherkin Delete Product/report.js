$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/DeleteProduct.feature");
formatter.feature({
  "name": "Delete Product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@delete"
    }
  ]
});
formatter.scenario({
  "name": "User wants to delete a product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@delete"
    }
  ]
});
formatter.step({
  "name": "User logged in and Create a Product",
  "keyword": "Given "
});
formatter.match({
  "location": "DeleteProduct.user_Created_a_Product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user navigate to product view",
  "keyword": "When "
});
formatter.match({
  "location": "DeleteProduct.user_navigate_to_product_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click on delete Product",
  "keyword": "And "
});
formatter.match({
  "location": "DeleteProduct.user_Click_on_delete_Product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will be directed to Daftar Jual Page and Product successfully deleted",
  "keyword": "Then "
});
formatter.match({
  "location": "DeleteProduct.user_will_be_directed_to_Daftar_Jual_Page_and_Product_successfully_deleted()"
});
formatter.result({
  "status": "passed"
});
});