$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/SearchProduct.feature");
formatter.feature({
  "name": "Search Product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@SearchProduct"
    }
  ]
});
formatter.scenario({
  "name": "SP01 - User search category 1",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP01"
    }
  ]
});
formatter.step({
  "name": "user is in secondhand Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_is_in_secondhand_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click on Hobi category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_click_on_Hobi_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with Hobi category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_Hobi_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP02 - User search category 2",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP02"
    }
  ]
});
formatter.step({
  "name": "user is in secondhand Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_is_in_secondhand_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click on Kendaraan category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_click_on_Kendaraan_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with Kendaraan category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_Kendaraan_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP03 - User search category 3",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP03"
    }
  ]
});
formatter.step({
  "name": "user is in secondhand Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_is_in_secondhand_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click on Baju category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_click_on_Baju_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with Baju category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_Baju_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP04 - User search category 4",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP04"
    }
  ]
});
formatter.step({
  "name": "user is in secondhand Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_is_in_secondhand_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click on Elektronik category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_click_on_Elektronik_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with Elektronik category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_Elektronik_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP05 - User search by keyword",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP05"
    }
  ]
});
formatter.step({
  "name": "user is in secondhand Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_is_in_secondhand_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user search baju in search product",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_search_baju_in_search_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with baju keyword",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_baju_keyword()"
});
formatter.result({
  "status": "passed"
});
});