$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/EditProfile.feature");
formatter.feature({
  "name": "Edit Profile",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@EditProfile"
    }
  ]
});
formatter.scenario({
  "name": "EP01 - User update profile without edit",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP01"
    }
  ]
});
formatter.step({
  "name": "User in Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.User_in_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EP02 - User edit profile",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP02"
    }
  ]
});
formatter.step({
  "name": "User in Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.User_in_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User edit profile",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_edit_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EP03 - User update profile with one blank field",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP03"
    }
  ]
});
formatter.step({
  "name": "User in Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.User_in_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User empty Name profile",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_empty_Name_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EP02 - update profile with empty in all fields",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP04"
    }
  ]
});
formatter.step({
  "name": "User in Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.User_in_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User empty all fileds",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_empty_all_fileds()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
});