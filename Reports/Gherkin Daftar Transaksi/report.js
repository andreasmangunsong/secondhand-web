$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/DaftarTransaksi.feature");
formatter.feature({
  "name": "Daftar Transaksi",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    }
  ]
});
formatter.scenario({
  "name": "DAF01 - Users wants to see the interaction or transaction of their product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    },
    {
      "name": "@DAF01"
    }
  ]
});
formatter.step({
  "name": "User login using an account that have product transaction",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarTransaksi.user_login_using_an_account_that_have_product_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User navigate to Daftar Jual/Transaksi",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarTransaksi.user_navigate_to_Daftar_Jual_Transaksi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will be shown the products which have an interaction or transaction",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarTransaksi.user_will_be_shown_the_products_which_have_an_interaction_or_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "DAF02 - Users does not have a product interaction or transaction",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    },
    {
      "name": "@DAF02"
    }
  ]
});
formatter.step({
  "name": "User login using an account that does not have a transaction",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarTransaksi.user_login_using_an_account_that_does_not_have_a_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User navigate to Daftar Jual/Transaksi",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarTransaksi.user_navigate_to_Daftar_Jual_Transaksi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will be shown message that tells no product interaction or transaction at the moment",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarTransaksi.user_will_be_shown_message_that_tells_no_product_interaction_or_transaction_at_the_moment()"
});
formatter.result({
  "status": "passed"
});
});