import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.maximizeWindow()

WebUI.click(findTestObject('Soneta/HomePage_SecondHand/btn_masuk'))

WebUI.verifyElementVisible(findTestObject('Soneta/LoginPage_SecondHand/label_masuk'))

WebUI.verifyElementVisible(findTestObject('Soneta/LoginPage_SecondHand/label_email', [('variable') : '']))

WebUI.verifyElementVisible(findTestObject('Soneta/LoginPage_SecondHand/label_password'))

WebUI.verifyElementVisible(findTestObject('Soneta/LoginPage_SecondHand/btn_login'))

WebUI.clearText(findTestObject('Soneta/LoginPage_SecondHand/textfield_email'))

WebUI.clearText(findTestObject('Soneta/LoginPage_SecondHand/textfield_password'))

WebUI.sendKeys(findTestObject('Soneta/LoginPage_SecondHand/textfield_email'), 'sonetachallenge@gmail.com')

WebUI.sendKeys(findTestObject('Soneta/LoginPage_SecondHand/textfield_password'), 'binar123456')

WebUI.click(findTestObject('Soneta/LoginPage_SecondHand/btn_login'))

WebUI.verifyElementVisible(findTestObject('Soneta/SellerPage_SecondHand/btn_profilicon'))

WebUI.click(findTestObject('Soneta/SellerPage_SecondHand/btn_listicon'))

WebUI.click(findTestObject('Soneta/ListIconPage_SecondHand/btn_cardtambahproduk'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/label_namaproduk'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/label_hargaproduk'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/label_kategori'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/label_deskripsi'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/textfield_namaproduk'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/textfield_hargaproduk'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/select_kategori'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/textfield_deskripsi'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/upload_picture'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/btn_terbitkan'))

WebUI.verifyElementVisible(findTestObject('Soneta/TambahProdukPage_SecondHand/btn_preview'))

