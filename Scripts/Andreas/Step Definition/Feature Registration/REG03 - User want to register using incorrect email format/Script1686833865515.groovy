import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Masuk Homepage'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Daftar Loginpage'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Nama Lengkap'), [('nama') : GlobalVariable.nama_def], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Password'), [('password') : GlobalVariable.password_def], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Daftar Registpage'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Veriify Failed'), [:], FailureHandling.STOP_ON_FAILURE)

