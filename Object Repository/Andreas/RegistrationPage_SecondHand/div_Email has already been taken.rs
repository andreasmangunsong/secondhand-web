<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Email has already been taken</name>
   <tag></tag>
   <elementGuidId>a93d6de8-dfa8-432f-a217-f85576e84ad9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='new_user']/div[2]/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-text.text-danger</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4f62f2d2-edfa-4291-83af-f5c73b162f33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text text-danger</value>
      <webElementGuid>d96789ee-04c5-418c-bde7-68c2417d6a7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Email has already been taken
              </value>
      <webElementGuid>aca4d632-b951-4c32-bd47-4378f8a73b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;field mb-4&quot;]/div[@class=&quot;form-text text-danger&quot;]</value>
      <webElementGuid>b3cf7fd1-1379-471f-97e4-4575d5b91bfc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[2]/div[3]</value>
      <webElementGuid>82a43498-cfae-49c3-af20-33a21ec735aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::div[2]</value>
      <webElementGuid>48262fb6-00f3-4805-ac24-57e8d662b8ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::div[4]</value>
      <webElementGuid>3f1265a8-01a1-4b3b-82c8-a21578d177fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::div[1]</value>
      <webElementGuid>7e850625-4e7f-46ea-b796-756f8ebed864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk di sini'])[1]/preceding::div[3]</value>
      <webElementGuid>38027d3a-cbe4-4bae-82b6-48cd480b95e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email has already been taken']/parent::*</value>
      <webElementGuid>90cfdb74-e04f-4bf2-a12a-ff02bc928052</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]</value>
      <webElementGuid>70d5c0b6-e0d0-4765-822e-0b052e943aeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Email has already been taken
              ' or . = '
                Email has already been taken
              ')]</value>
      <webElementGuid>c60aa888-3957-49af-abd4-4efc798a6229</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
