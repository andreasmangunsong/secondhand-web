<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Profil Saya</name>
   <tag></tag>
   <elementGuidId>95e5812c-43c9-4de6-80b6-fc19aa582814</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.dropdown-toggle.d-flex.align-items-center.show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0ff4b8b9-8d49-4faa-a8a7-84e3324e0644</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link dropdown-toggle d-flex align-items-center show</value>
      <webElementGuid>3aefcd0d-412d-4d6f-9f5a-6215dbdcf763</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>a0c642af-2fab-417f-824a-304cb0f7ef6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>47e40375-9408-47eb-a926-720871cc4a77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>5ac4b515-0fdb-4b3d-bc59-56dc08d6996d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>29932866-602a-4e8b-993a-2cc120adb030</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
      Profil Saya
    </value>
      <webElementGuid>ff7aef06-a410-4533-9216-413f22754ff1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown fs-5 d-none d-lg-block&quot;]/a[@class=&quot;nav-link dropdown-toggle d-flex align-items-center show&quot;]</value>
      <webElementGuid>aea7507e-49e4-42fa-bddb-2468b53b29fa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/a</value>
      <webElementGuid>bcd88ea0-1b61-4266-b7a5-0ccfbefaab74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/following::a[1]</value>
      <webElementGuid>b81db262-764d-4b64-ac07-3bded224ec14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[1]/following::a[1]</value>
      <webElementGuid>907f4adf-d4f9-49ac-ad3f-29df756b6f25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sultan'])[1]/preceding::a[1]</value>
      <webElementGuid>4fdbb1c8-4c3c-4e5f-8ffd-dea298864bdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[2]</value>
      <webElementGuid>8108b351-4dca-45d6-9e2a-fe39dbc76f2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/a</value>
      <webElementGuid>a4995bc4-9404-460c-a7c9-f86652e8844f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = '
      
      Profil Saya
    ' or . = '
      
      Profil Saya
    ')]</value>
      <webElementGuid>e28a22dd-808d-4bcb-81b1-6efbec0c6288</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
