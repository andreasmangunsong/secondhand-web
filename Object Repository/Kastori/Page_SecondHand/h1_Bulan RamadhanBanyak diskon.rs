<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Bulan RamadhanBanyak diskon</name>
   <tag></tag>
   <elementGuidId>d7e4ccb0-c63d-40a5-bc2f-fd8cc2061b42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.fw-bold.mb-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil berhasil diperbarui!'])[1]/following::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>f5a73fca-7a34-4ba1-96ef-584c30be123b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>e835aa11-4c5b-4a6a-a5e0-8fad3db6c445</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan RamadhanBanyak diskon!</value>
      <webElementGuid>b7a7436c-4f5f-49ac-8381-5dd5b3a78f63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;mt-lg-5 mb-5 w-100 d-flex gap-lg-5&quot;]/div[@class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;]/div[@class=&quot;row p-5&quot;]/div[@class=&quot;col&quot;]/h1[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>0c14ed5f-b678-462c-87a6-bfe75025317a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil berhasil diperbarui!'])[1]/following::h1[1]</value>
      <webElementGuid>930a9dd7-8f82-423f-95ab-0e3228b28bbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      <webElementGuid>6dc2be83-5eef-4861-9451-ddafa41c56e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/preceding::h1[1]</value>
      <webElementGuid>03297f7d-83c9-4fd3-bf20-4c4e197104da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan Ramadhan']/parent::*</value>
      <webElementGuid>4a93cc06-c09e-4ea5-a6be-28ffd785a739</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>f652802d-8f39-4ada-a8ef-ea43b49fe19b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Bulan RamadhanBanyak diskon!' or . = 'Bulan RamadhanBanyak diskon!')]</value>
      <webElementGuid>6e97e1ba-eb3c-4118-85ea-c75b1aacb486</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
