<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Masuk                              Emai_4d76ca</name>
   <tag></tag>
   <elementGuidId>29ce88e0-6af6-422e-90bf-a583c557a507</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.container.p-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1b51095e-8b59-456d-a77c-ce9a222a1393</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container p-5</value>
      <webElementGuid>21b2dfd1-b19b-4c52-bc46-996d2e976bf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      </value>
      <webElementGuid>ed271fd0-5971-4dbf-9f55-6ad7145a720d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;container-fluid min-height-100vh&quot;]/div[@class=&quot;row min-height-100vh w-100&quot;]/div[@class=&quot;col-6 d-flex flex-column align-items-center justify-content-center&quot;]/div[@class=&quot;container p-5&quot;]</value>
      <webElementGuid>aaa60528-ebe1-4811-936c-9c0bb26c36bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[2]</value>
      <webElementGuid>4967c2e3-9858-454c-b493-c2e714931628</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div</value>
      <webElementGuid>b69a786d-be41-4bf8-a66d-d82bc234d961</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      ' or . = '
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      ')]</value>
      <webElementGuid>97de76a1-7597-4f7e-83fc-bc4aff93a904</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
