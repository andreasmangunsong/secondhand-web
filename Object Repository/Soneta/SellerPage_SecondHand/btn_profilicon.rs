<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_profilicon</name>
   <tag></tag>
   <elementGuidId>cde9a9f0-47ad-438d-84c4-9530ab463c4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.dropdown-toggle.d-flex.align-items-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>477c60b1-8f83-4b0e-aca8-11ad1481ddab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link dropdown-toggle d-flex align-items-center</value>
      <webElementGuid>08dff646-0556-47ce-9449-83878abbc725</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>20131967-834c-423c-88c8-293b3c206b2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4441e22f-912b-47ff-807f-9453df810e8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>02120ed9-7f2d-4cb4-938a-a297269f1085</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
      Profil Saya
    </value>
      <webElementGuid>282f22f0-145e-49b7-ac6a-0a166a06bded</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown fs-5 d-none d-lg-block&quot;]/a[@class=&quot;nav-link dropdown-toggle d-flex align-items-center&quot;]</value>
      <webElementGuid>b337f6f5-5eea-4807-a8ab-e871432a9465</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/a</value>
      <webElementGuid>4aa9086b-2638-4538-acbc-bb1ea20834fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/following::a[1]</value>
      <webElementGuid>37cc46b7-7fc5-43a3-99ef-ecc8fd43aab4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[1]/following::a[1]</value>
      <webElementGuid>437ee1c8-9968-444f-8690-2a8e4fb021d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Challenge'])[1]/preceding::a[1]</value>
      <webElementGuid>77237731-76f3-4d34-8f20-3f633dcb2913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[2]</value>
      <webElementGuid>e934d5c8-0024-4a61-bc4e-8d2cd83c3d9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/a</value>
      <webElementGuid>23928818-1521-4c52-96d5-29c05acbbd4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = '
      
      Profil Saya
    ' or . = '
      
      Profil Saya
    ')]</value>
      <webElementGuid>9b5d19c3-f31b-4d14-a54f-72579dc6bbe0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
