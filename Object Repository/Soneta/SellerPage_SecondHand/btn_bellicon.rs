<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_bellicon</name>
   <tag></tag>
   <elementGuidId>6fbafb88-4b71-4ae1-989c-3892a628b8c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.nav-item.me-0.me-lg-2.fs-5.d-block.d-xl-none.position-relative > a.nav-link.d-flex.align-items-center</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'nav-link d-flex align-items-center' and @href = '/notifications' and (text() = '
      
      Notifikasi

' or . = '
      
      Notifikasi

')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>da9d4ecc-2a25-4e1f-b5ba-02e857dbb58e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex align-items-center</value>
      <webElementGuid>bdcb75b6-5cdc-407a-93f0-4f142b6763a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>628adbad-1154-46d5-afee-fc806649f24e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
      Notifikasi

</value>
      <webElementGuid>d8977857-e5e9-4e30-acfe-1821d2448991</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5 d-block d-xl-none position-relative&quot;]/a[@class=&quot;nav-link d-flex align-items-center&quot;]</value>
      <webElementGuid>da9e5df8-511c-4f79-9093-4b518d2af6c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[2]/a</value>
      <webElementGuid>47051381-10b5-4b44-b487-a49392df1167</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::a[1]</value>
      <webElementGuid>46f91f06-a355-4b37-82c7-302b61e13cd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[1]/preceding::a[2]</value>
      <webElementGuid>11123c46-e3ef-4812-bdf6-8a94f5ba0ab0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/notifications')]</value>
      <webElementGuid>7cca00eb-1e3d-4cc0-9be6-34922e23eed8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>d6bc4501-3137-4b1a-8e24-3103fc4654f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
      
      Notifikasi

' or . = '
      
      Notifikasi

')]</value>
      <webElementGuid>71dcba85-401e-4279-9018-96f4925f8a75</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
