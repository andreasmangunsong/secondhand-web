<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_whatsapp</name>
   <tag></tag>
   <elementGuidId>ce04db74-bb60-4699-bdf9-80219fc4379f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span._afw1 > a._9vcv > span._advp._aeam > img._afvz</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//header[@id='u_0_0_dn']/div/span[2]/a/span/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>3de6af89-c78b-4897-9f41-2219a7f926b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>_afvz</value>
      <webElementGuid>75a6f149-2710-489d-984a-d4fc7f944a01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>WhatsApp Main Page</value>
      <webElementGuid>da4ac53f-048b-4009-a831-9cf8e4bc0be3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://static.whatsapp.net/rsrc.php/v3/y7/r/DSxOAUB0raA.png</value>
      <webElementGuid>a0e80559-3a58-4cbb-b8d9-f5b4026d62b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;u_0_0_dn&quot;)/div[@class=&quot;_afvx&quot;]/span[@class=&quot;_afw1&quot;]/a[@class=&quot;_9vcv&quot;]/span[@class=&quot;_advp _aeam&quot;]/img[@class=&quot;_afvz&quot;]</value>
      <webElementGuid>6117fa41-a9c9-4b07-9847-ecdc476e6424</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//header[@id='u_0_0_dn']/div/span[2]/a/span/img</value>
      <webElementGuid>3dfd71fc-c18e-40ef-9d0d-9155898a22f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='WhatsApp Main Page'])[2]</value>
      <webElementGuid>d9ae5de2-31d2-4611-9da6-6e23a803de02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/a/span/img</value>
      <webElementGuid>ea21133e-2c20-40dd-82da-3b36d4568fae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'WhatsApp Main Page' and @src = 'https://static.whatsapp.net/rsrc.php/v3/y7/r/DSxOAUB0raA.png']</value>
      <webElementGuid>13674d14-93d5-45d2-bb97-cd054cb4c78a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
