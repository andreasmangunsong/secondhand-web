<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_cardtambahproduk</name>
   <tag></tag>
   <elementGuidId>17858e2f-1c68-4a7e-8c5c-3a404b26697c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.new-product-button.h-100.w-100.border-2.rounded-4.text-black-50.d-flex.align-items-center.justify-content-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>93fba818-0444-4cdf-a2d0-ba65e98b5c9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center</value>
      <webElementGuid>f6438b96-2b46-4e5a-9f53-fac9cdb0f613</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products/new</value>
      <webElementGuid>8dc6599c-9ae7-4c45-a3d8-d619fc631bbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
                
                Tambah Produk
              
</value>
      <webElementGuid>8253badc-d6b5-4bee-bb18-7d929d5504ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center&quot;]</value>
      <webElementGuid>004b3881-e78c-4fea-804f-b1a1d6e6d435</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::a[1]</value>
      <webElementGuid>33311118-2b22-40bd-8042-6cb45dc92aa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::a[2]</value>
      <webElementGuid>ef285fbf-0f6f-4b1c-ab75-efecb2c3a065</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products/new')]</value>
      <webElementGuid>8fef3e47-73c4-4ca7-8701-db2d90769bf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/a</value>
      <webElementGuid>dda3e128-d4f6-4750-bbdd-6f4fa5a368c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/new' and (text() = '
              
                
                Tambah Produk
              
' or . = '
              
                
                Tambah Produk
              
')]</value>
      <webElementGuid>d4d8550d-27ca-4311-ba09-00a39b387b61</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
