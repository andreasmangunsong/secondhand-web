<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Notifikasi</name>
   <tag></tag>
   <elementGuidId>55413ad6-efc2-4f54-8f99-39bf85500994</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.nav-item.me-0.me-lg-2.fs-5.d-block.d-xl-none.position-relative > a.nav-link.d-flex.align-items-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b61b11b1-d086-4f02-8056-011483d11efd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex align-items-center</value>
      <webElementGuid>0481218d-00a9-47f1-a8e1-bdf31a2fa012</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>d4cade36-3f2a-4874-8036-0c6571ebd4ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
      Notifikasi

</value>
      <webElementGuid>cfc10485-0ef7-4070-81d4-4d3b69c48e66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5 d-block d-xl-none position-relative&quot;]/a[@class=&quot;nav-link d-flex align-items-center&quot;]</value>
      <webElementGuid>5cddc53d-7e91-46b2-b1ec-fec245188c55</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[2]/a</value>
      <webElementGuid>178cffbc-ce62-40bc-b126-251ce100ae59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::a[1]</value>
      <webElementGuid>31952109-47f0-4dcc-88a7-7c861ca3bd58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[1]/preceding::a[2]</value>
      <webElementGuid>ba28cb7c-4907-4d39-b263-a303712b2d4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/notifications')]</value>
      <webElementGuid>ee15f933-3ed3-440a-895e-7ec8d9376971</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>5da6a85d-7451-4eae-955d-5ab3889922c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
      
      Notifikasi

' or . = '
      
      Notifikasi

')]</value>
      <webElementGuid>b1c11a83-afaf-4d75-b51d-511a239fccab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
