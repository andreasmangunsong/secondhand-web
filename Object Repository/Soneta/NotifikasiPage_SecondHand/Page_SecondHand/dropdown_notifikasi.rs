<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_notifikasi</name>
   <tag></tag>
   <elementGuidId>773f521c-f209-489b-bd76-e3677274aabc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.dropdown-menu.notification-dropdown-menu.px-4.show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>1f4dbca2-90d2-4cf5-b2a7-74bbf862b2fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-menu notification-dropdown-menu px-4 show</value>
      <webElementGuid>e37d8ad4-8516-41e3-bf9c-9446c8d6427b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-popper</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>c6887c5a-09a9-4f99-ad8d-4d5122a71d86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 12:44

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:56

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:24

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 10:54

          

            
          
            
                
              
                Penawaran produk
                Bawang WibuRp 123.122Ditawar Rp 123.456
              
              03 Jul, 10:01

          

            
          
            
                
              
                Berhasil di terbitkan
                Bawang WibuRp 123.122
              
              03 Jul, 10:00

          

            
          
            
                
              
                Berhasil di terbitkan
                sdadsRp 2.342
              
              03 Jul, 02:37

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 02:21

          

            
              
                Lihat semua notifikasi
            
    </value>
      <webElementGuid>d2b0b06d-dec3-4926-a8b6-c0faec5c62ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]</value>
      <webElementGuid>33cd2265-9f41-434d-9e91-cede2007f41d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      <webElementGuid>81c1228f-c5b5-4c7b-83e9-8870ab680bea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/following::ul[1]</value>
      <webElementGuid>41cfb0d5-145c-4ded-bc98-7eb3e0356f77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::ul[1]</value>
      <webElementGuid>7a0cc5b1-73dd-449f-9657-327fc90ea7db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul</value>
      <webElementGuid>e68493d4-a173-4077-b666-b35dad01d455</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 12:44

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:56

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:24

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 10:54

          

            
          
            
                
              
                Penawaran produk
                Bawang WibuRp 123.122Ditawar Rp 123.456
              
              03 Jul, 10:01

          

            
          
            
                
              
                Berhasil di terbitkan
                Bawang WibuRp 123.122
              
              03 Jul, 10:00

          

            
          
            
                
              
                Berhasil di terbitkan
                sdadsRp 2.342
              
              03 Jul, 02:37

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 02:21

          

            
              
                Lihat semua notifikasi
            
    ' or . = '
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 12:44

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:56

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:24

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 10:54

          

            
          
            
                
              
                Penawaran produk
                Bawang WibuRp 123.122Ditawar Rp 123.456
              
              03 Jul, 10:01

          

            
          
            
                
              
                Berhasil di terbitkan
                Bawang WibuRp 123.122
              
              03 Jul, 10:00

          

            
          
            
                
              
                Berhasil di terbitkan
                sdadsRp 2.342
              
              03 Jul, 02:37

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 02:21

          

            
              
                Lihat semua notifikasi
            
    ')]</value>
      <webElementGuid>91c4a625-4b4a-4b44-80e9-611d9da594ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
