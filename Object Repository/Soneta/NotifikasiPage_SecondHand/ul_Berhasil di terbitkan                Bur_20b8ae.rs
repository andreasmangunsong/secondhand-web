<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_Berhasil di terbitkan                Bur_20b8ae</name>
   <tag></tag>
   <elementGuidId>af1f638c-2c41-4987-a4f7-9be5d9e4daba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.dropdown-menu.notification-dropdown-menu.px-4.show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>8d5bc962-e8a3-4dc8-98fe-8e509d0314bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-menu notification-dropdown-menu px-4 show</value>
      <webElementGuid>645da488-d3e4-443d-ab0c-1c70485d66e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-popper</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>560e1a0c-01cf-4dc4-a731-8f22a76fe64f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 12:44

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:56

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:24

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 10:54

          

            
          
            
                
              
                Penawaran produk
                Bawang WibuRp 123.122Ditawar Rp 123.456
              
              03 Jul, 10:01

          

            
          
            
                
              
                Berhasil di terbitkan
                Bawang WibuRp 123.122
              
              03 Jul, 10:00

          

            
          
            
                
              
                Berhasil di terbitkan
                sdadsRp 2.342
              
              03 Jul, 02:37

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 02:21

          

            
              
                Lihat semua notifikasi
            
    </value>
      <webElementGuid>4b0f4127-d106-49d2-a53e-bb951eb3441d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]</value>
      <webElementGuid>a5ca8d0c-2022-41f0-9167-9ed7d9ee3720</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      <webElementGuid>55e7627e-299a-4973-b7ec-f8095d736b03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/following::ul[1]</value>
      <webElementGuid>9b16dab2-6397-471b-aa86-01abb23d3270</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::ul[1]</value>
      <webElementGuid>549453c6-55be-4d76-b78c-617bb7c95982</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul</value>
      <webElementGuid>f96a7e09-4f5a-4aa3-b750-2c024feb4bd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 12:44

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:56

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:24

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 10:54

          

            
          
            
                
              
                Penawaran produk
                Bawang WibuRp 123.122Ditawar Rp 123.456
              
              03 Jul, 10:01

          

            
          
            
                
              
                Berhasil di terbitkan
                Bawang WibuRp 123.122
              
              03 Jul, 10:00

          

            
          
            
                
              
                Berhasil di terbitkan
                sdadsRp 2.342
              
              03 Jul, 02:37

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 02:21

          

            
              
                Lihat semua notifikasi
            
    ' or . = '
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 12:44

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:56

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 11:24

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 10:54

          

            
          
            
                
              
                Penawaran produk
                Bawang WibuRp 123.122Ditawar Rp 123.456
              
              03 Jul, 10:01

          

            
          
            
                
              
                Berhasil di terbitkan
                Bawang WibuRp 123.122
              
              03 Jul, 10:00

          

            
          
            
                
              
                Berhasil di terbitkan
                sdadsRp 2.342
              
              03 Jul, 02:37

          

            
          
            
                
              
                Berhasil di terbitkan
                Burung WibuRp 123.456
              
              03 Jul, 02:21

          

            
              
                Lihat semua notifikasi
            
    ')]</value>
      <webElementGuid>e5fea1e5-0b85-4914-9720-f14045292970</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
