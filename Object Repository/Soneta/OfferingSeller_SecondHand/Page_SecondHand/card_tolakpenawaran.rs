<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_tolakpenawaran</name>
   <tag></tag>
   <elementGuidId>3ab8718f-7e4f-459b-b974-99a80a0df4bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li > a.notification.my-4.px-2.position-relative</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d883c72e-e353-4fdf-b39a-a9d6c3f617e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>e696a0b6-666c-4e2e-9e13-94d1c1f28023</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>17842</value>
      <webElementGuid>ff6faea6-5837-4fff-a454-c26dc85e4446</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a2569696-ae11-412e-b0bb-f47115cab1d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/users/8399/offers</value>
      <webElementGuid>3ceafb3c-e362-4ee5-946b-837b8c110d04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
            
              Penawaran produk
              apoloRp 1.234.561Ditawar Rp 123.434
            
            03 Jul, 15:13

</value>
      <webElementGuid>c7ee24c2-1d98-4938-a15f-697411d1f994</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>3796a8b5-240d-4dc6-b1b1-facae17863bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[1]/following::a[1]</value>
      <webElementGuid>28237c99-b5d4-4e43-a444-d1b7298923c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/users/8399/offers')])[4]</value>
      <webElementGuid>b89369c1-1bd3-4f25-b8c2-45312e9ad50c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li/a</value>
      <webElementGuid>31f69be1-54c0-4f09-aa5a-05fc7b40f280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/users/8399/offers' and (text() = '
              
            
              Penawaran produk
              apoloRp 1.234.561Ditawar Rp 123.434
            
            03 Jul, 15:13

' or . = '
              
            
              Penawaran produk
              apoloRp 1.234.561Ditawar Rp 123.434
            
            03 Jul, 15:13

')]</value>
      <webElementGuid>8f7e7e83-9d17-4fae-b472-61c6c6ec309e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
