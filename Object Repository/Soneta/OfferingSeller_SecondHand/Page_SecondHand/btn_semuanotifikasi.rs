<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_semuanotifikasi</name>
   <tag></tag>
   <elementGuidId>55e70588-1aaa-4e4b-a64e-009a63b310e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-link.text-decoration-none</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[@class=&quot;p-2 text-center&quot;]/a[@class=&quot;btn btn-link text-decoration-none&quot;][count(. | //*[@class = 'btn btn-link text-decoration-none' and @href = '/notifications' and (text() = '
                Lihat semua notifikasi
' or . = '
                Lihat semua notifikasi
')]) = count(//*[@class = 'btn btn-link text-decoration-none' and @href = '/notifications' and (text() = '
                Lihat semua notifikasi
' or . = '
                Lihat semua notifikasi
')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[16]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e9ffe554-be5e-4859-b8cf-1df4d708be6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link text-decoration-none</value>
      <webElementGuid>44843c7d-ce3d-45f7-b7a1-90c3077de0c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>7370e180-d698-48f4-8d71-ed20d4c351d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Lihat semua notifikasi
</value>
      <webElementGuid>9220d071-ae06-4a80-ab73-75b982045950</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[@class=&quot;p-2 text-center&quot;]/a[@class=&quot;btn btn-link text-decoration-none&quot;]</value>
      <webElementGuid>1192f7f7-4a01-4c46-9a5f-e5f49395bdad</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[16]/a</value>
      <webElementGuid>c878b62d-a0d9-426b-aeaa-8f35c4b20786</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat semua notifikasi')]</value>
      <webElementGuid>b47c222d-0fb3-4ee1-8365-da87e4cea72f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Burung WibuRp 123.456'])[5]/following::a[1]</value>
      <webElementGuid>fc5ad316-c82a-4f66-b725-cbff36115df0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[7]/following::a[1]</value>
      <webElementGuid>acba7443-f3ef-4bfc-8b2b-e2b69dd99b1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[1]/preceding::a[1]</value>
      <webElementGuid>6c3d58ee-d294-4adb-8a0d-778e9eadc4e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/preceding::a[2]</value>
      <webElementGuid>47d3b12f-8274-4f26-9cc0-16b766e97280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lihat semua notifikasi']/parent::*</value>
      <webElementGuid>204fc3fb-05b9-4ed1-84d3-012b4b53c05b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/notifications')])[2]</value>
      <webElementGuid>2f33b474-780a-4258-8122-772cc69483e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[16]/a</value>
      <webElementGuid>b38cfc27-b047-41fa-87a9-76990afdf089</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
                Lihat semua notifikasi
' or . = '
                Lihat semua notifikasi
')]</value>
      <webElementGuid>0670684e-cb44-4f44-a3f4-4d379d854328</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
