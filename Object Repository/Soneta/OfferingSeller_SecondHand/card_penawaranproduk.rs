<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_penawaranproduk</name>
   <tag></tag>
   <elementGuidId>89134b92-3993-477b-ba76-31e132dee61a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li:nth-of-type(9) > a.notification.my-4.px-2.position-relative</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Burung WibuRp 123.456'])[9]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a3d4902e-4c98-484c-b5a2-5dca86d379b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>0d3847c3-8ccd-4e70-83c1-51939bec42e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>17788</value>
      <webElementGuid>2c57eb25-578a-44c9-ba34-38329820cfff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b5a618ee-39f4-4d45-9574-3f81aadbf018</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/users/8399/offers</value>
      <webElementGuid>1b5bf0ea-a37f-4e36-b32d-335805186a96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
            
              Penawaran produk
              Bawang WibuRp 123.122Ditawar Rp 123.456
            
            03 Jul, 10:01

</value>
      <webElementGuid>9abb9215-8508-4c41-9488-7aedac7badd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[9]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>ad2e6270-0d90-40e6-8c52-a6f52f402fec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Burung WibuRp 123.456'])[9]/following::a[1]</value>
      <webElementGuid>e3dc2f10-cf19-42bb-97a1-ee4f8eb188e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[11]/following::a[1]</value>
      <webElementGuid>7b206b66-61f0-47e7-a064-b29a7bd67d6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/users/8399/offers')])[2]</value>
      <webElementGuid>901ede4e-9986-4fab-8df0-9671429a743b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[9]/a</value>
      <webElementGuid>d5cda794-b703-453d-a43f-340ffcf0a349</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/users/8399/offers' and (text() = '
              
            
              Penawaran produk
              Bawang WibuRp 123.122Ditawar Rp 123.456
            
            03 Jul, 10:01

' or . = '
              
            
              Penawaran produk
              Bawang WibuRp 123.122Ditawar Rp 123.456
            
            03 Jul, 10:01

')]</value>
      <webElementGuid>f401f0cf-d7af-449d-ba45-d7c82fc2f2d5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
