<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_whatsapp</name>
   <tag></tag>
   <elementGuidId>85f40a6d-6fae-4e35-bd75-0c81ada1c332</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-primary.fw-bold.rounded-pill.px-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Hubungi di')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f2cdfb4f-91bf-4045-bec2-0ea257370df9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>3ce249d8-b076-4932-a6b5-09dc8f093bc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary fw-bold rounded-pill px-4</value>
      <webElementGuid>c419b26d-ea1c-4cfc-ae5b-280f2e983363</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://wa.me/7868678678</value>
      <webElementGuid>84762b8a-f4a0-4b60-a10b-6bb5000d836e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Hubungi di 
</value>
      <webElementGuid>d318ea19-d983-44ce-ad8a-8669720f1efa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;offers mt-5&quot;]/li[@class=&quot;offer gap-4 mt-5&quot;]/div[@class=&quot;offer-content&quot;]/div[@class=&quot;offer-footer d-flex gap-2 justify-content-end&quot;]/a[@class=&quot;btn btn-primary fw-bold rounded-pill px-4&quot;]</value>
      <webElementGuid>5ab89719-abd5-4ee7-a915-ee4425a0b9bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Hubungi di')]</value>
      <webElementGuid>86d793eb-3974-4d0d-a767-cac3f2665e89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::a[1]</value>
      <webElementGuid>0fab18ad-2580-4020-9e99-2c031adf9489</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 123.456'])[1]/following::a[1]</value>
      <webElementGuid>8c1c22c0-b023-48df-96b3-754136c0a3d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perbarui status penjualan produkmu'])[1]/preceding::a[1]</value>
      <webElementGuid>aecab515-c859-47e6-8a9f-f4d81d8ec619</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil terjual'])[1]/preceding::a[1]</value>
      <webElementGuid>0d7860fe-209e-4ebc-9aa4-4cf247b61930</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Hubungi di']/parent::*</value>
      <webElementGuid>270000e4-5c2b-45b8-a71a-6a6438f84d50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://wa.me/7868678678')]</value>
      <webElementGuid>0aa5950e-455a-4bef-9170-3bef512234c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a</value>
      <webElementGuid>d56acdfe-2b78-4ecd-9034-3967ad46e877</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://wa.me/7868678678' and (text() = '
                Hubungi di 
' or . = '
                Hubungi di 
')]</value>
      <webElementGuid>af118516-8b8c-4d5e-be63-6d65568faed1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
