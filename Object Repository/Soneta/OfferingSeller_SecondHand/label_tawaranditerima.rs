<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_tawaranditerima</name>
   <tag></tag>
   <elementGuidId>bd859c4b-cfa2-4669-8b31-139760546b52</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h6.offer-status.text-black-50</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Produkmu yang Ditawar'])[1]/following::h6[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>23350ff4-a34e-4b15-8ba3-caac76f7013a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>offer-status text-black-50</value>
      <webElementGuid>aeb01520-d766-4dc7-9d11-e27c368ddaac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              Penawaran produk diterima
            </value>
      <webElementGuid>d97fe73d-6f51-4551-8a93-cc433b9dabfc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;offers mt-5&quot;]/li[@class=&quot;offer gap-4 mt-5&quot;]/div[@class=&quot;offer-content&quot;]/div[@class=&quot;offer-header&quot;]/h6[@class=&quot;offer-status text-black-50&quot;]</value>
      <webElementGuid>0b5ca449-ff7c-49af-8ffc-524d602d5477</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Produkmu yang Ditawar'])[1]/following::h6[1]</value>
      <webElementGuid>dced965a-dce9-4dfa-ac05-afd573541615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[1]/following::h6[1]</value>
      <webElementGuid>fcbbd5b0-7bbe-4125-916d-3b278f26c4c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bawang Wibu'])[1]/preceding::h6[1]</value>
      <webElementGuid>5021df63-ff60-4d03-ae1b-4e09424133cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 123.122'])[1]/preceding::h6[1]</value>
      <webElementGuid>185f72b7-7295-4b0d-ac85-45507886fb14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Penawaran produk diterima']/parent::*</value>
      <webElementGuid>4392c64d-81eb-4177-943c-058a6e20d05a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h6</value>
      <webElementGuid>c5f059d6-be7c-4d86-9cf4-b1d440403e8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
              Penawaran produk diterima
            ' or . = '
              Penawaran produk diterima
            ')]</value>
      <webElementGuid>593e391b-3e65-4da7-b20e-d2913334690b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
