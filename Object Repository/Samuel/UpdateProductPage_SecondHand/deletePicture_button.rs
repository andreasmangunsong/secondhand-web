<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deletePicture_button</name>
   <tag></tag>
   <elementGuidId>dcfc0189-4599-4b6d-9797-7c89a6b347fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn-close.btn-close-white.position-absolute.top-0.start-100.translate-middle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8bb22aed-93fc-4874-96ac-f43631509d59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>df027129-5a47-4c10-952e-a034863ddb12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>this.parentElement.remove()</value>
      <webElementGuid>b793fdee-eedd-4bcd-a66e-a250b931a6b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn-close btn-close-white position-absolute top-0 start-100 translate-middle</value>
      <webElementGuid>b7c728f1-c343-41a5-9146-8180e9416e2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>close</value>
      <webElementGuid>31ca26e7-c314-430f-b976-20851d5c0277</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input-images&quot;)/div[@class=&quot;form-image-preview position-relative rounded-4 overflow-hidden&quot;]/button[@class=&quot;btn-close btn-close-white position-absolute top-0 start-100 translate-middle&quot;]</value>
      <webElementGuid>49270176-0fb6-4269-8134-a9bdcbd81403</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[2]</value>
      <webElementGuid>b1093971-c531-403c-a0d0-e59715a80bfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='input-images']/div/button</value>
      <webElementGuid>58778c89-b267-4cf1-8870-4f12f4bb326a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/button</value>
      <webElementGuid>b0cbf4af-97c9-48e7-8b29-6db5675b44db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button']</value>
      <webElementGuid>4a5bfff9-058e-4a90-8e25-4426101749aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
