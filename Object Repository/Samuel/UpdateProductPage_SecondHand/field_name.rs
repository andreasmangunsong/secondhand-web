<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_name</name>
   <tag></tag>
   <elementGuidId>fc492259-124b-4758-b884-393d3f1a1900</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#product_name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='product_name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1237e5ff-ad44-49e1-bb0b-83c1edebf421</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>b0d68a85-4821-4c4c-b91b-e181a6d34cab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Nama Produk</value>
      <webElementGuid>ebd7fdca-be29-420e-867a-d87c9dd19442</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>17364124-da28-449c-b3ef-75174ea9c4f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>91a7f2a1-6592-4b30-9954-fd442084472f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Kipas Angin</value>
      <webElementGuid>62551aa6-e794-4ebd-b41c-596989efe9c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>product[name]</value>
      <webElementGuid>14e86f56-ecd5-48e3-a46c-2abd4e7cf6a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_name</value>
      <webElementGuid>10a3bfb4-3ff9-40ca-9704-121df85e6f2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product_name&quot;)</value>
      <webElementGuid>3bf31fca-6104-4481-9b47-9ad742acb6a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='product_name']</value>
      <webElementGuid>c056e498-8c9a-41b3-b69d-5bc94c70903c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/form/div/input</value>
      <webElementGuid>502b4aa7-b9c5-4c7d-be75-f04e45f43b12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Nama Produk' and @type = 'text' and @name = 'product[name]' and @id = 'product_name']</value>
      <webElementGuid>48c81f58-17e3-4f16-968e-6ab87d037652</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
