<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_description</name>
   <tag></tag>
   <elementGuidId>256030a6-a66d-4400-a02a-e382dd81652d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#product_description</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='product_description']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>3589df2f-c206-4367-96f6-912541319e06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>3eaefd76-0007-49c8-8ce1-b76c97a9c3f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: Jalan Ikan Hiu 33</value>
      <webElementGuid>4a7e8f16-e73e-4e8c-a67d-f53a30383093</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>f95a3141-bd55-4af4-ae71-aae4f7fa11bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>product[description]</value>
      <webElementGuid>db377915-e253-4d78-85de-66e3fba1500d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_description</value>
      <webElementGuid>bd375801-7770-4dcd-bdbc-0505e214cbef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kipas Angin</value>
      <webElementGuid>53a6b1db-099d-4a3e-8615-f791e410212b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product_description&quot;)</value>
      <webElementGuid>3b1b56b8-c414-484d-9798-fae0d753b33a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='product_description']</value>
      <webElementGuid>d886bc1d-97e6-422c-9fe2-f40018bbe58e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::textarea[1]</value>
      <webElementGuid>63f25b4a-1072-42de-bb0c-e5758861b888</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::textarea[1]</value>
      <webElementGuid>4e94fb28-2ae8-4470-abab-5ef17b921ee7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::textarea[1]</value>
      <webElementGuid>46c40a6e-7e5d-4249-a668-3f35e05742ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terbitkan'])[1]/preceding::textarea[1]</value>
      <webElementGuid>4b5c10fa-36c7-44f9-935d-709106f399e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>bae7c596-036d-4d89-bdd7-cbfd34ac241d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Contoh: Jalan Ikan Hiu 33' and @name = 'product[description]' and @id = 'product_description' and (text() = 'Kipas Angin' or . = 'Kipas Angin')]</value>
      <webElementGuid>3592af4c-bc23-4494-a985-47f8bfa8766f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
