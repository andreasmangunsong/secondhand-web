<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password_actions</name>
   <tag></tag>
   <elementGuidId>5e98b16d-e68e-401d-8426-4526f40ba9bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.actions</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='new_user']/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>db147a68-c1bc-4a87-ae5a-0da57c99dde4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>actions</value>
      <webElementGuid>18c9d06e-37fe-4d6f-ba87-f48c1a1d54e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;actions&quot;]</value>
      <webElementGuid>1e68442c-c9d6-4f93-a36c-9b03e4d95714</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[3]</value>
      <webElementGuid>ed093a61-61e0-4498-8db2-5dfadeff7e78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::div[1]</value>
      <webElementGuid>22b2e412-36c1-45a5-8d83-98278f330e33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::div[2]</value>
      <webElementGuid>f0d5c806-82e6-49ea-9dcb-15c5f86cab6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar di sini'])[1]/preceding::div[1]</value>
      <webElementGuid>37257b5e-4906-4c4d-a367-393fa47e2df9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>8568c0ea-a611-44ed-a0f9-be73d55cb778</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
