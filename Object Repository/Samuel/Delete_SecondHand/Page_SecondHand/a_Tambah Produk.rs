<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Tambah Produk</name>
   <tag></tag>
   <elementGuidId>2dc154d1-59f0-4e0b-a9b2-f242b0640b0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.new-product-button.h-100.w-100.border-2.rounded-4.text-black-50.d-flex.align-items-center.justify-content-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2d8e6290-778d-43e8-aed4-72114c96c555</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center</value>
      <webElementGuid>411e193a-dce6-466b-84dc-b27b3e20167e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products/new</value>
      <webElementGuid>33552993-ab37-47dc-8ce3-a19ed2cec20f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
                
                Tambah Produk
              
</value>
      <webElementGuid>326665d0-ca6f-46c8-a876-93f30dbe4885</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center&quot;]</value>
      <webElementGuid>3e9892b0-e9dd-420b-beba-159c08d9f6b0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::a[1]</value>
      <webElementGuid>e3f645e3-7c9c-41e0-abca-71d74fc3506a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::a[2]</value>
      <webElementGuid>735fd5c9-3b77-4ede-96cf-7cee79ba20aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kipas Angin Turbo'])[1]/preceding::a[1]</value>
      <webElementGuid>08d06e2f-f406-4d16-92c5-49b5a6c6ef2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products/new')]</value>
      <webElementGuid>8f9353c1-565d-483c-939c-8485834a94b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/a</value>
      <webElementGuid>61312b09-8201-44bc-86d4-93a76ad328f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/new' and (text() = '
              
                
                Tambah Produk
              
' or . = '
              
                
                Tambah Produk
              
')]</value>
      <webElementGuid>4fc0aedd-0d3f-4e77-9049-357efc0ce1aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
