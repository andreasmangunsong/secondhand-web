<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Semua Produk</name>
   <tag></tag>
   <elementGuidId>789146df-6cdd-4d12-bbef-1d5b27ca6ea9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.d-flex.py-3.border-bottom.border-1.active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>16c9d97e-3798-4ed8-a58f-8a002decc631</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex py-3 border-bottom border-1 active</value>
      <webElementGuid>26b492fb-1900-4ba1-804e-3808bc92d3ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products</value>
      <webElementGuid>6436766e-8456-4c37-9079-65822b14ae6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
              Semua Produk
              
</value>
      <webElementGuid>08284d24-ed77-419b-8d1d-d43f87bc59c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-3&quot;]/div[@class=&quot;card border-0 rounded-4 shadow p-4&quot;]/ul[@class=&quot;nav flex-column&quot;]/li[@class=&quot;nav-item&quot;]/a[@class=&quot;nav-link d-flex py-3 border-bottom border-1 active&quot;]</value>
      <webElementGuid>2820c14f-a76d-4f3e-9c1a-24e4c0a8d216</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::a[1]</value>
      <webElementGuid>f8059afe-5e84-4028-b337-d49239a64491</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::a[1]</value>
      <webElementGuid>ac6b55ef-b2e5-4257-a267-a0ebcad894d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/preceding::a[1]</value>
      <webElementGuid>1d723411-5e41-4b1c-9519-4bf66be34da5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/products')])[8]</value>
      <webElementGuid>61460208-7fb5-4566-b189-a3ddf1a4a9c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li/a</value>
      <webElementGuid>79ed8fe6-bda7-42cf-99ec-323b7f8a1adc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products' and (text() = '
              
              Semua Produk
              
' or . = '
              
              Semua Produk
              
')]</value>
      <webElementGuid>1c9df7bf-1ac6-4785-a8ae-52a781d0f3c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
