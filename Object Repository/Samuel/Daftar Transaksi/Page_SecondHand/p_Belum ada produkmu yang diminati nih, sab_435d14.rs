<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Belum ada produkmu yang diminati nih, sab_435d14</name>
   <tag></tag>
   <elementGuidId>03e2d709-2899-4db1-93c2-ee1f9232a011</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.py-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::p[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok' or . = 'Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>d9696ed0-e3d2-4f52-95d8-a4285b9a9e5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>py-5</value>
      <webElementGuid>862d6802-dc5d-4997-9f4e-09edc22e6f36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok</value>
      <webElementGuid>e6cc1597-86e5-4008-b1ed-d3b19fbee87a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;d-flex flex-column justify-content-center align-items-center&quot;]/div[@class=&quot;container d-flex flex-column justify-content-center align-items-center&quot;]/p[@class=&quot;py-5&quot;]</value>
      <webElementGuid>1a26741e-93d1-47ef-becf-fc9b8507e500</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::p[1]</value>
      <webElementGuid>e0dd2065-f36d-44f1-b2d8-3cfb3ab54372</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::p[1]</value>
      <webElementGuid>d185ede1-b367-4f60-9947-9c9243065b7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok']/parent::*</value>
      <webElementGuid>6459d667-8d28-4092-94b0-5cfe305b5c1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>80f9c908-0abf-43b2-9e4c-28fc9849f9db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok' or . = 'Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok')]</value>
      <webElementGuid>6e44628c-61df-48ba-ab44-2c0e0d1f184e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
