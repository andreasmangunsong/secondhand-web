<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Jual Produk A2</name>
   <tag></tag>
   <elementGuidId>cbd860b2-80ef-4e84-8d6d-546c1e0c1ee2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::h5[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body.text-decoration-none > h5.card-title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>0ca82016-2a0f-42d5-86e4-fa3a7dcb4395</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title</value>
      <webElementGuid>753bfc2b-59e4-4942-b298-a2c6867e256e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jual Produk A2</value>
      <webElementGuid>e7d628d7-ca9a-47a4-980b-37e533a7ec1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]/h5[@class=&quot;card-title&quot;]</value>
      <webElementGuid>ce8dcde4-bbbd-4213-8a2f-f828bcc508a8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::h5[1]</value>
      <webElementGuid>0c6fdd50-f067-42b6-aa33-7659a1d59dcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::h5[1]</value>
      <webElementGuid>6fddedb8-9708-44b5-9fd9-331b2a105682</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 7.834.953'])[1]/preceding::h5[1]</value>
      <webElementGuid>8a39af38-0a31-4daf-99bb-980c42baef7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div/h5</value>
      <webElementGuid>65c2e922-929c-462a-9aff-7298cf508a1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Jual Produk A2' or . = 'Jual Produk A2')]</value>
      <webElementGuid>0154c24e-a186-4a95-96fa-cd150a9f5f4e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
