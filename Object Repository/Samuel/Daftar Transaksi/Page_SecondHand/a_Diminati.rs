<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Diminati</name>
   <tag></tag>
   <elementGuidId>32e42b94-c21e-4603-b02d-f49ba01e6248</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua Produk'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>facc064c-66f9-403c-9c52-7c5d3b0e9409</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex py-3 border-bottom border-1</value>
      <webElementGuid>14a5e1c6-f8b7-4a35-b390-ec9e7b03dcb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products?product%5Bliked%5D=true&amp;product%5Bsold%5D=false</value>
      <webElementGuid>8758fae9-7ce7-4403-be9c-c9409cb23d49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
              Diminati
              
</value>
      <webElementGuid>277f79b6-a667-4bf0-858a-ef67db47db43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-3&quot;]/div[@class=&quot;card border-0 rounded-4 shadow p-4&quot;]/ul[@class=&quot;nav flex-column&quot;]/li[@class=&quot;nav-item&quot;]/a[@class=&quot;nav-link d-flex py-3 border-bottom border-1&quot;]</value>
      <webElementGuid>59319479-2146-4635-be56-35f8f81e4315</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua Produk'])[1]/following::a[1]</value>
      <webElementGuid>8dfc4c80-d2a5-4045-8eb8-16bcbc0854d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::a[2]</value>
      <webElementGuid>ed8e7e01-ccd2-444a-9154-d8b7f524335a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/preceding::a[1]</value>
      <webElementGuid>6497d2af-d564-4b1a-a572-4da79a09552b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products?product%5Bliked%5D=true&amp;product%5Bsold%5D=false')]</value>
      <webElementGuid>b1b4422f-a36e-4617-9824-e34354ae35fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>c188517a-e74b-4657-8364-55e2a3882df1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products?product%5Bliked%5D=true&amp;product%5Bsold%5D=false' and (text() = '
              
              Diminati
              
' or . = '
              
              Diminati
              
')]</value>
      <webElementGuid>22834553-6307-4c3e-8b82-d8b854f067ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
