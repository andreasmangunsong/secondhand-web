<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Belum ada produkmu yang diminati nih, s_cc5059</name>
   <tag></tag>
   <elementGuidId>2abdb656-2037-4bbc-aa1b-747311dfa9f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.container.d-flex.flex-column.justify-content-center.align-items-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2ed3a7f0-d63a-4719-ab51-4b41023b57f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container d-flex flex-column justify-content-center align-items-center</value>
      <webElementGuid>1d9d19a3-1411-409f-a01b-a1a03207ac28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
              Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok
            </value>
      <webElementGuid>5b0e12cd-3336-4187-b441-85ad70e548ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;d-flex flex-column justify-content-center align-items-center&quot;]/div[@class=&quot;container d-flex flex-column justify-content-center align-items-center&quot;]</value>
      <webElementGuid>1cda9da9-c6a8-4b4d-8e70-c4331694a968</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::div[4]</value>
      <webElementGuid>d70b9691-1b02-4f7e-81be-b882e12dd4c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::div[4]</value>
      <webElementGuid>fa5e3f8a-300f-4e96-b3f0-775d24e4cbcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div</value>
      <webElementGuid>90f992f1-5058-4b92-a325-7df739607cb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
              
              Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok
            ' or . = '
              
              Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok
            ')]</value>
      <webElementGuid>5fc490a7-bf48-4320-bc22-c37f4f939154</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
