<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DescriptionBlankValidation</name>
   <tag></tag>
   <elementGuidId>0eeec617-8615-462a-9e59-bae8579c120e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>848e0b26-87ee-4676-8e16-36d64e5d3805</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text text-danger</value>
      <webElementGuid>8485535e-ac6b-4493-a1f9-34521ec35103</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Description can't be blank
          </value>
      <webElementGuid>df783bd3-ea85-4eef-9b8f-7fb1b71a0777</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container container-sm position-relative&quot;]/section[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-11&quot;]/form[1]/div[@class=&quot;mb-4&quot;]/div[@class=&quot;form-text text-danger&quot;]</value>
      <webElementGuid>4a5c05a2-2cc1-44f0-9771-5e6cf9770ac0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::div[2]</value>
      <webElementGuid>b80c3619-6bbd-4ce2-ad72-d9351ad777a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Category must exist'])[1]/following::div[4]</value>
      <webElementGuid>0a5e741a-667c-4bc8-b461-350645a44fdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::div[4]</value>
      <webElementGuid>f6bd7867-234c-4910-9ec4-0c596a031099</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terbitkan'])[1]/preceding::div[4]</value>
      <webElementGuid>1c306477-809a-4fec-bdaf-8076efaf5d68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]</value>
      <webElementGuid>f42856da-4cc2-47d3-98d7-1769308fef3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
            Description can&quot; , &quot;'&quot; , &quot;t be blank
          &quot;) or . = concat(&quot;
            Description can&quot; , &quot;'&quot; , &quot;t be blank
          &quot;))]</value>
      <webElementGuid>aa2c7da5-7d75-4f32-9194-bce9a9e50f4a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
