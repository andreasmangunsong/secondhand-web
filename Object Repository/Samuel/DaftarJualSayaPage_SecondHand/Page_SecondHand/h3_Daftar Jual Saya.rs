<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Daftar Jual Saya</name>
   <tag></tag>
   <elementGuidId>a2f69cb6-0ed7-4dc5-8909-eba6e3bfb643</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.fw-bold.my-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::h3[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>158aca69-123c-41d3-969c-bde7743c4975</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold my-5</value>
      <webElementGuid>113dbc87-afcb-4dfb-bcf8-719fcff9a061</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Jual Saya</value>
      <webElementGuid>4ddcd453-bcb9-4d7b-ac16-7416fadb76e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/h3[@class=&quot;fw-bold my-5&quot;]</value>
      <webElementGuid>3d74ab44-773c-4dba-b8c3-10cabf94610d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::h3[1]</value>
      <webElementGuid>42ac7ed3-fbf1-4ff0-a9a5-a077c2be7d76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Samuel'])[2]/preceding::h3[1]</value>
      <webElementGuid>982e3f6c-bdc6-49fd-9ea4-4d660b1f92fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[2]/preceding::h3[1]</value>
      <webElementGuid>51a421bd-9ebb-4fe7-9558-29ce589ffb91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Jual Saya']/parent::*</value>
      <webElementGuid>2cd959fb-f287-4ee8-8240-be16780894b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>36648e78-b635-4950-b323-7a85262f4e92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Daftar Jual Saya' or . = 'Daftar Jual Saya')]</value>
      <webElementGuid>019cd68e-64dd-4885-9935-8b5cd6f74593</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
