<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Masuk</name>
   <tag></tag>
   <elementGuidId>01b174bd-f083-4854-b80c-71f74196ec3e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5d56d08f-ddcd-46e9-b06a-9da50a64dc64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>afb6031e-8a52-4995-a05b-c1a37186aa08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/users/sign_in</value>
      <webElementGuid>422020bd-961e-4470-bd04-6f7bf3fee5e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  
  Masuk
</value>
      <webElementGuid>500acc07-97f1-4b35-b777-cd7c6365a2c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/a[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>5150479a-4c69-4fcd-b2b6-18c7540de234</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/a</value>
      <webElementGuid>05200ce8-3967-40d8-8db9-cc8fbb553da3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan RamadhanBanyak diskon!'])[1]/preceding::a[1]</value>
      <webElementGuid>bcc41dc6-b976-4a12-887d-ffef5bac6a46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::a[1]</value>
      <webElementGuid>5098044e-bf36-45a7-a9e5-96e2e89d7b50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masuk']/parent::*</value>
      <webElementGuid>543df727-b7dc-40cf-9ed4-2b273bdf25f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/users/sign_in')]</value>
      <webElementGuid>de1d8f2e-e7a1-4367-b895-6e5240ed38a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/a</value>
      <webElementGuid>0ea7bdfa-a735-4f14-8332-6f8aa8580490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/users/sign_in' and (text() = '
  
  Masuk
' or . = '
  
  Masuk
')]</value>
      <webElementGuid>b0fb1868-caef-4135-83c3-738044900d1c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
