<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_aasdf</name>
   <tag></tag>
   <elementGuidId>2caed669-d677-49db-b725-8076959a0d5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-body.text-decoration-none > h5.card-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Produk'])[1]/following::h5[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'aasdf' or . = 'aasdf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>7f2c3e05-2a39-405a-8a5f-c26366fa133d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title</value>
      <webElementGuid>3fa8d9b8-ea1d-48f7-a5c9-b4d5fea759a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>aasdf</value>
      <webElementGuid>135ff0bf-2bd5-4c1c-9b64-139c29386cec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]/h5[@class=&quot;card-title&quot;]</value>
      <webElementGuid>44ca012d-a283-49da-9570-6b214489ab13</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Produk'])[1]/following::h5[1]</value>
      <webElementGuid>a3d938d0-6196-4153-87f1-2c7adb5c48d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::h5[2]</value>
      <webElementGuid>6a794c84-5896-4eb6-9ab6-b8385f65d821</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 88'])[1]/preceding::h5[1]</value>
      <webElementGuid>164a7564-feed-4e70-8a27-89daba55fc9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div/h5</value>
      <webElementGuid>a784040b-3752-47d6-a0a0-03b776f2bc88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'aasdf' or . = 'aasdf')]</value>
      <webElementGuid>9881f2a3-4108-4101-b7da-83d790d60269</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
