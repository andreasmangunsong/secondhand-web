<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Jual</name>
   <tag></tag>
   <elementGuidId>39a7e486-ea9d-42b2-889b-4e73da6cf690</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-primary.btn-lg.rounded-4.d-inline-flex.align-items-center.justify-content-space-between.px-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Next →'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7b65f8ae-ed17-4eab-8561-59930d9492d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg rounded-4 d-inline-flex align-items-center justify-content-space-between px-5</value>
      <webElementGuid>cd62d50f-aed5-4677-a020-3a43254fbcc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products/new</value>
      <webElementGuid>a0c017c2-37f4-4020-8af9-518b3dd8ceb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    Jual
</value>
      <webElementGuid>76633065-96c4-4b1c-88f5-05055f721e1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;sticky-bottom d-flex align-items-center justify-content-center py-4&quot;]/a[@class=&quot;btn btn-primary btn-lg rounded-4 d-inline-flex align-items-center justify-content-space-between px-5&quot;]</value>
      <webElementGuid>60c72d4c-14fc-4cb2-bc5c-3527c7fe3f6f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next →'])[1]/following::a[1]</value>
      <webElementGuid>67dc7b4b-4091-4807-8dca-a8358d730d6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='← Previous'])[1]/following::a[2]</value>
      <webElementGuid>d69cb398-95bc-49da-8c34-4dbfb719a0b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products/new')]</value>
      <webElementGuid>25f9c2e4-ba15-4556-8102-283428abc5ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/a</value>
      <webElementGuid>72104070-2188-465e-86bd-801044de1254</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/new' and (text() = '
    
    Jual
' or . = '
    
    Jual
')]</value>
      <webElementGuid>e4c9f677-d4d8-44ab-88e5-e2c03b5004bb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
