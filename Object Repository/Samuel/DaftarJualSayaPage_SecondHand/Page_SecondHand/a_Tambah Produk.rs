<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Tambah Produk</name>
   <tag></tag>
   <elementGuidId>e6015045-d6b3-4e19-9280-06e09f2c2c6b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.new-product-button.h-100.w-100.border-2.rounded-4.text-black-50.d-flex.align-items-center.justify-content-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b24b6218-bfba-46b8-965b-548ffb16d410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center</value>
      <webElementGuid>d2feebcb-5f22-4717-8aaa-3b80fad77bfc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products/new</value>
      <webElementGuid>6f1c1e0b-e661-4ce7-bb36-02e66cc917aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
                
                Tambah Produk
              
</value>
      <webElementGuid>38d3d5b3-8787-4b8b-b0e7-9de0faf321c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center&quot;]</value>
      <webElementGuid>61f71b07-8473-4ead-9b30-dacb6e656ba0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::a[1]</value>
      <webElementGuid>0cd5795b-3be0-41b6-9c31-fa8584f9871a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::a[2]</value>
      <webElementGuid>4d946e4e-5c57-4708-b20a-ed5f91680ab1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jual Produk A2'])[1]/preceding::a[1]</value>
      <webElementGuid>9821464c-5304-4713-9b50-775335a18831</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products/new')]</value>
      <webElementGuid>cdee1bef-d401-4223-80b1-4871989dd1b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/a</value>
      <webElementGuid>c30155e7-66d8-4392-9a7a-456b385cb8b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/new' and (text() = '
              
                
                Tambah Produk
              
' or . = '
              
                
                Tambah Produk
              
')]</value>
      <webElementGuid>b92684fe-5df6-4d09-97fe-78af6ac28a06</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
