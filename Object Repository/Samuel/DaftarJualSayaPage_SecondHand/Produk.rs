<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Produk</name>
   <tag></tag>
   <elementGuidId>fb47c1e3-f10c-4064-80fd-fd471be8575b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.px-0.border-0.shadow.h-100.pb-4.rounded-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Produk'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f87ae03b-c243-4c44-b767-9c0d8aaf2421</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>780f3f8f-4408-4dc3-9ce6-e17e15379c52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      Kipas Angin Turbo
      Baju
      Rp 112.333
    
  </value>
      <webElementGuid>53c4db90-fa8e-4584-9c0b-7ba1989209de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>5bd52913-dff4-4117-9bc8-7ba37e5b89a9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Produk'])[1]/following::div[2]</value>
      <webElementGuid>56081d6f-95d4-4a38-9d26-83fd3caab589</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::div[6]</value>
      <webElementGuid>66c26daf-6630-4543-951e-492179f2afca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/div</value>
      <webElementGuid>73df84db-1db1-460a-87a1-5af1058ab362</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      Kipas Angin Turbo
      Baju
      Rp 112.333
    
  ' or . = '
      

    
      Kipas Angin Turbo
      Baju
      Rp 112.333
    
  ')]</value>
      <webElementGuid>687f6148-0a99-4ee1-8d88-796ee447d466</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
