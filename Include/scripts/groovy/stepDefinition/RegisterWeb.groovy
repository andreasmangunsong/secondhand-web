package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class RegisterWeb {
	@Given("user is in the secondhand LoginPage")
	public void user_is_in_the_secondhand_LoginPage() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Masuk Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user click link Daftar")
	public void user_click_link_Daftar() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Daftar Loginpage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input Name {string}")
	public void user_input_Name(String nama) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Nama Lengkap'), [('nama') : GlobalVariable.nama_def],
		FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input Email {string}")
	public void user_input_Email(String email) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Random Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input Password {string}")
	public void user_input_Password(String password) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Password'), [('password') : GlobalVariable.password_def],
		FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input incorrect Name {string}")
	public void user_input_incorrect_Name(String wrongName) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Nama Lengkap'), [('nama') : wrongName],
		FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input used Email {string}")
	public void user_input_used_Email(String usedEmail) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Email'), [('email') : GlobalVariable.email_def],
		FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input incorrect Email {string}")
	public void user_input_incorrect_Email(String wrongEmail) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Email'), [('email') : wrongEmail], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input incorrect Password {string}")
	public void user_input_incorrect_Password(String wrongPass) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Input Password'), [('password') : wrongPass], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click button Daftar")
	public void user_click_button_Daftar() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Daftar Registpage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user redirected to HomePage")
	public void user_redirected_to_HomePage() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Verify Success'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("warning message appear")
	public void warning_message_appear() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Veriify Failed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("error email taken")
	public void error_email_taken() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Verify Email Already Used'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}