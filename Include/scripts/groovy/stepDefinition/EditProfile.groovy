package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProfile {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User in Home Page")
	public void User_in_Home_Page() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.url)
	}

	@When("User login into the account")
	public void User_login_into_the_account() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on Profile button")
	public void User_click_on_Profile_button() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Click User Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User edit profile")
	public void User_edit_profile() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Edit with -'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User empty Name profile")
	public void User_empty_Name_profile() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Empty Username'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User empty all fileds")
	public void User_empty_all_fileds() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Empty Username'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Empty Address'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Empty Phone Number'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Click Simpan'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Verify Empty'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Simpan button")
	public void User_click_Simpan_button() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Click Simpan'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.closeBrowser();
	}
}