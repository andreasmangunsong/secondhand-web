package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginWeb {
	@Given("user is in secondhand LoginPage")
	public void user_is_in_secondhand_LoginPage() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Click Masuk Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input email {string}")
	public void user_input_email(String email) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Input Email'), [('email') : GlobalVariable.email_def], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input password {string}")
	public void user_input_password(String password) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Input Password'), [('password') : GlobalVariable.password_def],
		FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input incorrect email {string}")
	public void user_input_incorrect_email(String wrongEmail) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Input Email'), [('email') : wrongEmail], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input incorrect password {string}")
	public void user_input_incorrect_password(String wrongPass) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Input Password'), [('password') : wrongPass], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click button MASUK")
	public void user_click_button_MASUK() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Click Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to HomePage")
	public void user_will_redirected_to_HomePage() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Verify Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will failed to login")
	public void user_will_failed_to_login() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Verify Empty'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("error message appear")
	public void error_message_appear() {
		WebUI.callTestCase(findTestCase('Andreas/Function/Login Page/Verify Login Failed'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@Given("user open the browser")
	public void user_open_the_browser() {
		WebUI.openBrowser('')
	}

	@And("user enter secondhand web")
	public void user_enter_secondhand_web() {
		WebUI.navigateToUrl(GlobalVariable.url)
	}

	@And("user close the browser")
	public void user_close_the_browser() {
		WebUI.closeBrowser()
	}
}