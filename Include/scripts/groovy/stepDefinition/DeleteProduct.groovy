package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.apache.logging.log4j.core.appender.rolling.action.DeleteAction
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class DeleteProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User logged in and Create a Product")
	public void user_Created_a_Product() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Add a product to be deleted'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("user navigate to product view")
	public void user_navigate_to_product_view() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Navigate to product View'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@And("User Click on delete Product")
	public void user_Click_on_delete_Product() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Delete a Product'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be directed to Daftar Jual Page and Product successfully deleted")
	public void user_will_be_directed_to_Daftar_Jual_Page_and_Product_successfully_deleted() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Verify Daftar Jual page'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}