package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SearchProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user is in secondhand Homepage")
	public void user_is_in_secondhand_Homepage() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.url)
	}

	@When("user click on Hobi category button")
	public void user_click_on_Hobi_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Click_Category1'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click on Kendaraan category button")
	public void user_click_on_Kendaraan_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Click_Category2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click on Baju category button")
	public void user_click_on_Baju_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Click_Category3'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click on Elektronik category button")
	public void user_click_on_Elektronik_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Click_Category4'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user search baju in search product")
	public void user_search_baju_in_search_product() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Search By Keyword'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with baju keyword")
	public void user_will_redirected_to_product_page_with_baju_keyword() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Verify_Search by Keyword'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with Hobi category")
	public void user_will_redirected_to_product_page_with_Hobi_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Verify_Category1'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with Kendaraan category")
	public void user_will_redirected_to_product_page_with_Kendaraan_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Verify_Category2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with Baju category")
	public void user_will_redirected_to_product_page_with_Baju_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Verify_Category3'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with Elektronik category")
	public void user_will_redirected_to_product_page_with_Elektronik_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Product/Verify_Category4'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}