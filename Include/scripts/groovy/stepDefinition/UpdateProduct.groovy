package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class UpdateProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user successfully log in to the web")
	public void user_successfully_log_in_to_the_web() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user successfully navigated and log in to the web")
	public void user_successfully_navigated_and_log_in_to_the_web() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/Navigate User Login'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user navigated to product detail view")
	public void user_navigated_to_product_detail_view() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Navigate to Update Product'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input product name")
	public void user_input_product_name() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/RandomNamaProduct'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input product price with {string}")
	public void user_input_product_price_with(String hargaProduk) {
		WebUI.callTestCase(findTestCase('Test Cases/Samuel/Pages/Update Product/Input Product Price'), [('hargaProduk') : hargaProduk] , FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input product category")
	public void user_input_product_category() {
		WebUI.callTestCase(findTestCase('Test Cases/Samuel/Pages/Update Product/Input Product Category'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input product description with {string}")
	public void user_input_product_description_with(String deskripsiProduk) {
		WebUI.callTestCase(findTestCase('Test Cases/Samuel/Pages/Update Product/Input Product Description'),  [('deskripsiProduk') : deskripsiProduk], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input product image")
	public void user_input_product_image() {
		WebUI.callTestCase(findTestCase('Test Cases/Samuel/Pages/Update Product/Input Product Picture'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click button preview")
	public void user_click_button_preview() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Click Preview'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click button terbitkan")
	public void user_click_button_terbitkan() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Click Terbitkan'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click button terbitkan from product view")
	public void user_click_button_terbitkan_from_product_view() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Click Terbitkan di View Product'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product Updated and user directed to product view")
	public void user_directed_to_product_view() {
		WebUI.callTestCase(findTestCase('Test Cases/Samuel/Pages/Update Product/Verify View Product Page'), null, FailureHandling.STOP_ON_FAILURE)
		WebUI.closeBrowser()
	}

	@When("user input product name with {string}")
	public void user_input_product_name_with(String namaProduk) {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Name'), [('namaProduk') : namaProduk], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user does not input product category")
	public void user_does_not_input_product_category() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Empty Category'), null, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("error message will appear to validate empty fields")
	public void error_message_will_appear_to_validate_empty_fields() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Verify Empty Fields'), null, FailureHandling.STOP_ON_FAILURE)
	}
}