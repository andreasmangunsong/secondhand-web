#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@login @smoke
Feature: Login
  As a user, I want to login to secondhand web

  @loginValid @LOG01 @loginInvalid @LOG02
  Scenario: LOG02 - User want to login without input email and password
    Given user open the browser
    And user enter secondhand web
    And user is in secondhand LoginPage
    Then user click button MASUK
    And user will failed to login
    And user close the browser

  @loginInvalid @LOG03
  Scenario Outline: LOG03 - User want to login using incorrect credential
    Given user open the browser
    And user enter secondhand web
    And user is in secondhand LoginPage
    When user input incorrect email "<email>"
    And user input incorrect password "<password>"
    Then user click button MASUK
    And error message appear
    And user close the browser

    Examples: 
      | email                | password  |
      | binarteam2@gmail.com | wrongPass |
      | wrongEmail@gmail.com | binar0202 |
      | wrongEmail@gmail.com | wrongPass |

  Scenario: LOG01 - User want to login using correct credential
    Given user open the browser
    And user enter secondhand web
    And user is in secondhand LoginPage
    When user input email "binarteam2@gmail.com"
    And user input password "binar0202"
    Then user click button MASUK
    And user will redirected to HomePage
    And user close the browser
