#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Register @smoke
Feature: Register
  As a user, I want to register to secondhand web

  @RegisterInvValid @REG02
  Scenario Outline: REG02 - User want to register using incorrect credential
    Given user open the browser
    And user enter secondhand web
    And user is in the secondhand LoginPage
    And user click link Daftar
    When user input incorrect Name "<name>"
    And user input incorrect Email "<email>"
    And user input incorrect Password "<password>"
    Then user click button Daftar
    And warning message appear
    And user close the browser

    Examples: 
      | name    | email          | password  |
      | binar02 | andreasmailcom | binar0202 |
      |         |                |           |

  @RegisterInvValid @REG03
  Scenario: REG03 - User want to register using registered email
    Given user open the browser
    And user enter secondhand web
    And user is in the secondhand LoginPage
    And user click link Daftar
    When user input Name "Andreas"
    And user input used Email "binarteam2@gmail.com"
    And user input Password "Andreas123"
    Then user click button Daftar
    And error email taken
    And user close the browser

  @RegisterValid @REG01
  Scenario: REG01 - User want to register using correct credential
    Given user open the browser
    And user enter secondhand web
    And user is in the secondhand LoginPage
    And user click link Daftar
    When user input Name "andreas"
    And user input Email "randomEmail"
    And user input Password "binar0202"
    Then user click button Daftar
    And user redirected to HomePage
    And user close the browser
