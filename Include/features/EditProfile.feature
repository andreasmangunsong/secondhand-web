@EditProfile
Feature: Edit Profile

  @EP01
  Scenario: EP01 - User update profile without edit
    Given User in Home Page
    When User login into the account
    Then User click on Profile button
    Then User click Simpan button

  @EP02
  Scenario: EP02 - User edit profile
    Given User in Home Page
    When User login into the account
    Then User click on Profile button
    Then User edit profile
    Then User click Simpan button

  @EP03
  Scenario: EP03 - User update profile with one blank field
    Given User in Home Page
    When User login into the account
    Then User click on Profile button
    Then User empty Name profile
    Then User click Simpan button
    
  @EP04
  Scenario: EP04 - update profile with empty in all fields
    Given User in Home Page
    When User login into the account
    Then User click on Profile button
    Then User empty all fileds
    Then User click Simpan button
    
 
