Feature: Update Product
	User wants to edit a product they have published before

	@UpdateProduct @UPD01 @positive
  Scenario: UPD01 - User wants Update their Product with valid data
	Given user successfully log in to the web
	When user navigated to product detail view
	When user input product name
	And user input product price with "456000"
	And user input product category
	And user input product description with "edit positive"
	And user input product image
	And user click button preview
	And user click button terbitkan from product view
	Then Product Updated and user directed to product view

	@UpdateProduct @UPD02 @negative
  Scenario: UPD02 - User wants to update without enter any data
	Given user successfully navigated and log in to the web
	When user navigated to product detail view
	When user input product name with ""
	And user input product price with ""
	And user does not input product category
	And user input product description with ""
	And user click button terbitkan
	Then error message will appear to validate empty fields

	@UpdateProduct @UPD03 @negative
  Scenario: UPD03 - User wants to update product, but input a negative number in the price field
	Given user successfully navigated and log in to the web
	When user navigated to product detail view
	When user input product name with "Harga negative"
	And user input product price with "-4321"
	And user input product category
	And user input product description with "test harga negative"
	And user click button terbitkan
	Then Product Updated and user directed to product view