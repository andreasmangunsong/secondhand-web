Feature: Add product as seller

    
@AddingProduct @AP01
 	Scenario: Add Product Valid Data
 		 Given user successfully log in to the web
     Then user direct to tambah produk
 		 When user input product name "Burung"
		 And user input product price with "1234561"
		 And user input product category "Hobi"
		 And user input product description with "Burung nih"
		 And user input product image "Burung.jpeg"
		 And user click button preview
		 And user click button terbitkan
	   Then user directed to product view
	   
@AddingProduct @AP02
 	Scenario: Add Product invalid Data
 		 Given user successfully log in to the web
     Then user direct to tambah produk
 		 When user input product name ""
		 And user input product price with ""
		 And user input product category ""
		 And user input product description with ""
		 And user input product image ""
		 And user click button preview
		 And user click button terbitkan
	   Then error message will appear to validate empty fields
