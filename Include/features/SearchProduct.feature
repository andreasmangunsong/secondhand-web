@SearchProduct
Feature: Search Product

	@SP01
  Scenario: SP01 - User search category 1
    Given user is in secondhand Homepage
    When user click on Hobi category button
    Then user will redirected to product page with Hobi category
    
  @SP02
  Scenario: SP02 - User search category 2
    Given user is in secondhand Homepage
    When user click on Kendaraan category button
    Then user will redirected to product page with Kendaraan category
        
  @SP03
  Scenario: SP03 - User search category 3
    Given user is in secondhand Homepage
    When user click on Baju category button
    Then user will redirected to product page with Baju category
        
  @SP04
  Scenario: SP04 - User search category 4
    Given user is in secondhand Homepage
    When user click on Elektronik category button
    Then user will redirected to product page with Elektronik category
    
  @SP05
  Scenario: SP05 - User search by keyword
    Given user is in secondhand Homepage
    When user search baju in search product
    Then user will redirected to product page with baju keyword