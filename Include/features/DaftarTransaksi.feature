#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@DaftarTransaksi
Feature: Daftar Transaksi

	@DAF01
  Scenario: DAF01 - Users wants to see the interaction or transaction of their product
    Given User login using an account that have product transaction
    When User navigate to Daftar Jual/Transaksi
    Then User will be shown the products which have an interaction or transaction

  @DAF02
	Scenario: DAF02 - Users does not have a product interaction or transaction
    Given User login using an account that does not have a transaction
    When User navigate to Daftar Jual/Transaksi
    Then User will be shown message that tells no product interaction or transaction at the moment